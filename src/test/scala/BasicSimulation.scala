import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

class BasicSimulation extends Simulation {

  val httpConf = http
    .baseURL("http://viktors-macbook-pro.local:4516/")
    .contentTypeHeader("application/json")
    .basicAuth("admin", "admin")

  val scn = scenario("BasicSimulation")
    .exec(http("Create release from template")
    .post("releases")
    .body(StringBody(
    """
          {
             "title":"Performance test 101",
             "description":null,
             "owner":{
                "username":"admin",
                "fullName":"XL Release Administrator"
             },
             "scheduledStartDate":"2014-08-22T07:00:00.000Z",
             "dueDate":"2014-08-22T15:00:00.000Z",
             "plannedDuration":null,
             "variables":[
                {
                   "key":"admin",
                   "value":"admin",
                   "type":"DEFAULT"
                }
             ],
             "tags":[
                "tutorial"
             ],
             "flag":{
                "status":"OK"
             },
             "abortOnFailure":false,
             "templateId":"ReleaseTemplate_tour"
          }
    """.stripMargin

  ))
    .check(status.is(200))
    .check(jsonPath("$.id").saveAs("RELEASEID"))

    )

  val addPhaseToRelease = scenario("Add phases to release")
    .exec((http("Add phase to a release"))
    .post("releases/${RELEASEID}/phases/add")
    .body(StringBody(
    "{}".stripMargin))
  .check(status.is(200))
  .check(jsonPath("$.id").saveAs("PHASEID")))


  val removePhaseFromRelease = scenario("Remove phase from release")
  .exec((http("Remove phase from a release"))
  .delete("phases/${PHASEID}")
  .check(status.is(204)))


  val rondje = scenario("Test").exec(scn).exec(addPhaseToRelease).exec(removePhaseFromRelease)


  setUp(
    rondje.inject(
      rampUsers(500) over(60 seconds),
      constantUsersPerSec(200) during(60 seconds),
      heavisideUsers(10000) over(30 seconds)
    ).protocols(httpConf))
}